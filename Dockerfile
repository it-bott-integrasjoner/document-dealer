# `python-base` sets up all our shared environment variables
FROM harbor.uio.no/mirrors/docker.io/library/python:3.11-slim as python-base

LABEL no.uio.contact=bott-int-drift@usit.uio.no

# python
ENV PYTHONUNBUFFERED=1 \
    # prevents python creating .pyc files
    PYTHONDONTWRITEBYTECODE=1 \
    \
    # paths
    # this is where our requirements + virtual environment will live
    PYSETUP_PATH="/opt/pysetup" \
    VENV_PATH="/opt/pysetup/.venv"

# prepend poetry and venv to path
ENV PATH="${POETRY_HOME}/bin:${VENV_PATH}/bin:${PATH}"

# `builder-base` stage is used to build deps + create our virtual environment
FROM python-base as builder-base

RUN export DEBIAN_FRONTEND=noninteractive \
    && apt-get update \
    && apt-get install -y --no-install-recommends git

ARG POETRY_VERSION=1.5

ENV PIP_NO_CACHE_DIR=1 \
    PIP_DISABLE_PIP_VERSION_CHECK=1 \
    PIP_DEFAULT_TIMEOUT=100 \
    # poetry
    # https://python-poetry.org/docs/configuration/#using-environment-variables
    POETRY_VERSION=${POETRY_VERSION} \
    POETRY_VIRTUALENVS_CREATE=false

ENV PATH="${POETRY_HOME}/bin:${VENV_PATH}/bin:${PATH}"

WORKDIR ${PYSETUP_PATH}

RUN pip3 install poetry=="${POETRY_VERSION}"

WORKDIR /document-dealer
COPY . /document-dealer

RUN poetry install --only main --no-interaction --no-ansi


RUN chmod 774 /document-dealer
