import unittest

from document_dealer import config

import yaml


class Test(unittest.TestCase):
    def test_get_config(self):
        with open("config.example.yaml") as f:
            config_fixture = config.Config(
                **yaml.load(f.read(), Loader=yaml.FullLoader)
            )

        c = config.get_config(filename="config.example.yaml")

        self.assertEqual(c, config_fixture)
