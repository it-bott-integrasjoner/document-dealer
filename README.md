# Document Dealer deals documents

Document Dealer collects documents from DFØs document service, and presents the
documents for the requestors in-line. The requestors visits DD by clicking an
URL in e.g. Tableau.

## Example usage:

Run Document Dealer:

```
$ poetry shell
$ poetry install
$ cp config.example.yaml config.yaml
$ vim config.yaml
$ DOCUMENT_DEALER_CONFIG=config.yaml uvicorn document_dealer.main:app --reload
```

Collect a document:

```
$ xdg-open 'http://127.0.0.1:8000/document?firm=72&doc_id=6bdf3104-88e3-4707-9398-9f4175b58939&doc_type=INNGFAKTVEDL&revision=0'
```

## Running tests:

```
$ coverage run -m unittest
```

### pre-commit

Activate [pre-commit](https://pre-commit.com/) hooks:

```
$ pre-commit install
```

## Pipelines:

Gitlab has now been setup to build and push new docker images to harbor, for new commits on master and for new tags/releases.

You can still build docker images locally with the update-harbor-image.sh script (mentioned below).

But if you want the script to also push the built images to harbor, you now have to set the CI environment variable to some value when you run it, like this:

`CI=true update-harbor-image.sh`
