# TABLE APPLSYS.FND_LOBS
#
# +-------------------+--------------------+----------+
# | COLUMN_NAME       | DATA_TYPE          | NULLABLE |
# +===================+====================+==========+
# | FILE_ID           | NUMBER             | No       |
# +-------------------+--------------------+----------+
# | FILE_NAME         | VARCHAR2(256 BYTE) | Yes      |
# +-------------------+--------------------+----------+
# | FILE_CONTENT_TYPE | VARCHAR2(256 BYTE) | No       |
# +-------------------+--------------------+----------+
# | FILE_DATA         | BLOB               | Yes      |
# +-------------------+--------------------+----------+
# | UPLOAD_DATE       | DATE               | Yes      |
# +-------------------+--------------------+----------+
# | EXPIRATION_DATE   | DATE               | Yes      |
# +-------------------+--------------------+----------+
# | PROGRAM_NAME      | VARCHAR2(32 BYTE)  | Yes      |
# +-------------------+--------------------+----------+
# | PROGRAM_TAG       | VARCHAR2(32 BYTE)  | Yes      |
# +-------------------+--------------------+----------+
# | LANGUAGE          | VARCHAR2(4 BYTE)   | Yes      |
# +-------------------+--------------------+----------+
# | ORACLE_CHARSET    | VARCHAR2(30 BYTE)  | Yes      |
# +-------------------+--------------------+----------+
# | FILE_FORMAT       | VARCHAR2(10 BYTE)  | No       |
# +-------------------+--------------------+----------+

from document_dealer.oracle_bot.types import Query


def query(file_id: int) -> Query:
    sql = """
            SELECT FILE_NAME, FILE_CONTENT_TYPE, FILE_DATA
            FROM APPLSYS.FND_LOBS
            WHERE FILE_ID = :file_id
            OFFSET 0 ROWS FETCH NEXT 1 ROWS ONLY
            """
    parameters = {"file_id": file_id}
    return Query(sql=sql, parameters=parameters)
