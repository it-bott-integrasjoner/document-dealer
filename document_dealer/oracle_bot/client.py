import functools
import pathlib

import oracledb

import document_dealer.oracle_bot.fnd_lobs as fnd_lobs
import document_dealer.oracle_bot.streamserve as streamserve
from document_dealer.oracle_bot.types import OracleConfig, Reply

# returns strings or bytes instead of a locator
# See https://python-oracledb.readthedocs.io/en/latest/user_guide/lob_data.html
oracledb.defaults.fetch_lobs = False


class OracleClient:
    def __init__(self, config: OracleConfig):
        self.config = config

    @property
    def _connection(self) -> oracledb.Connection:
        return self._connection_pool.acquire()

    @functools.cached_property
    def _connection_pool(self) -> oracledb.ConnectionPool:
        # TODO: Sjekk at dette er en grei måte å gjøre det på.
        return oracledb.create_pool(  # type: ignore[no-any-return]
            dsn=self.config.dsn,
            user=self.config.user,
            password=self.config.password,
            min=2,  # TODO
            max=2,  # TODO
        )

    def get_fnd_lobs_document(self, file_id: int) -> Reply | None:
        query = fnd_lobs.query(file_id)
        conn = self._connection
        with conn.cursor() as cursor:
            cursor.execute(query.sql, parameters=query.parameters)
            if row := cursor.fetchone():
                file_name, file_content_type, file_data = row
                if file_name is not None:
                    # We get the full path from the database, return just the base name
                    file_name = pathlib.Path(file_name).name
                return Reply(
                    document=file_data,
                    filename=file_name,
                    mimetype=file_content_type,
                )
        return None

    def get_streamserve_document(self, *, firm: str, fakturanr: int) -> Reply | None:
        query = streamserve.query(fakturanr=fakturanr, firma=firm)
        conn = self._connection
        with conn.cursor() as cursor:
            cursor.execute(query.sql, parameters=query.parameters)
            if row := cursor.fetchone():
                file_content_type, file_data = row
                return Reply(
                    document=file_data,
                    filename=None,
                    mimetype=file_content_type,
                )
        return None
