# VIEW STRSARCHIVE.FAKTURA_WEBSERVICE
#
# +-----------------+----------------+----------+
# | COLUMN_NAME     | DATA_TYPE      | NULLABLE |
# +=================+================+==========+
# | FAKTURANR       | NUMBER(28,8)   | Yes      |
# +-----------------+----------------+----------+
# | SET_OF_BOOKS_ID | NUMBER         | Yes      |
# +-----------------+----------------+----------+
# | DOKUMENT        | BLOB           | Yes      |
# +-----------------+----------------+----------+
# | DOKTYPE         | NVARCHAR2(400) | Yes      |
# +-----------------+----------------+----------+
# | PARTID          | RAW            | No       |
# +-----------------+----------------+----------+
# | OPPRETTET_DATO  | DATE           | Yes      |
# +-----------------+----------------+----------+
# | DFO_FIRMAKODE   | VARCHAR2(2)    | Yes      |
# +-----------------+----------------+----------+

from document_dealer.oracle_bot.types import Query


# FAKTURANR is NUMBER(28,8), but all values are guaranteed to be integers
def query(*, fakturanr: int, firma: str) -> Query:
    sql = """
            SELECT DOKTYPE, DOKUMENT
            FROM STRSARCHIVE.FAKTURA_WEBSERVICE
            WHERE FAKTURANR = :fakturanr AND DFO_FIRMAKODE = :firma
            """
    parameters = {"fakturanr": fakturanr, "firma": firma}
    return Query(sql=sql, parameters=parameters)
