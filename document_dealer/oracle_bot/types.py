from enum import StrEnum
from typing import NamedTuple, Any

import pydantic


class Query(NamedTuple):
    sql: str
    parameters: dict[str, Any]


class SourceSystem(StrEnum):
    FND_LOBS = "fnd_lobs"
    STREAMSERVE = "streamserve"


class OracleConfig(pydantic.BaseModel):
    dsn: str
    user: str
    password: str


class Reply(NamedTuple):
    document: bytes
    filename: str | None
    mimetype: str
