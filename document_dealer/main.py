import hashlib
import datetime
import time
import urllib.parse
from functools import lru_cache

import fastapi
import logging.config
from typing import Optional, assert_never

from document_dealer import oracle_bot
from .document_client import DocumentClient
from .oracle_bot import OracleClient, OracleConfig
from .config import get_config, AuthConfig, InstanceConfig

app = fastapi.FastAPI(docs_url=None, redoc_url=None)

config = get_config()
logger = logging.getLogger(__name__)


def configure(raise_on_error: bool) -> None:
    configure_logging(raise_on_error=raise_on_error)
    configure_sentry(raise_on_error=raise_on_error)


def configure_logging(raise_on_error: bool) -> None:
    import logging.config

    if logging_cfg := config.logging:
        try:
            logging.config.dictConfig(logging_cfg)
        except Exception as exc:  # pragma: no cover
            if raise_on_error:
                raise
            logging.basicConfig()
            logger.exception("Error configuring logging", exc_info=exc)
    else:
        logging.basicConfig()


def configure_sentry(raise_on_error: bool) -> None:
    if sentry_cfg := config.sentry:
        logger.info("Sentry enabled, initializing")
        try:
            import sentry_sdk

            sentry_sdk.init(**sentry_cfg.dict(exclude_unset=True))
            logger.info("Sentry initialized")
        except Exception as exc:  # pragma: no cover
            if raise_on_error:
                raise
            logger.exception("Error configuring Sentry", exc_info=exc)
    else:
        logger.info("Sentry configuration not found")


configure(raise_on_error=False)


def get_instance_config(instance: str) -> Optional[InstanceConfig]:
    return config.instance_config.get(instance)


def generate_token(config: AuthConfig, identity: str) -> str:
    date = datetime.date.strftime(datetime.date.today(), "%Y-%m-%d")
    m = hashlib.sha256()
    m.update(f"{date}{config.key}{identity}".encode("utf-8"))
    return m.hexdigest()


async def authorised(firm: str, token: str, doc_id: str) -> bool:
    instance_config = get_instance_config(firm)
    if instance_config is None:
        return False
    generated_token = generate_token(instance_config.auth_config, doc_id).upper()
    return bool(token and token == generated_token)


@lru_cache
def get_oracle_client(firm: str) -> OracleClient | None:
    instance_config = get_instance_config(firm)
    if (
        instance_config is None
        or not instance_config.old_bot_enabled
        or config.old_bot is None
    ):
        return None

    oracle_config = OracleConfig(
        dsn=config.old_bot.database.dsn,
        user=config.old_bot.database.user,
        password=config.old_bot.database.password,
    )
    return OracleClient(oracle_config)


@app.get("/document")
async def get_document(
    firm: str,
    doc_id: str,
    doc_type: str,
    revision: int,
    authorised: bool = fastapi.Depends(authorised),
) -> fastapi.responses.Response:
    if not authorised:
        return fastapi.responses.PlainTextResponse(
            status_code=401, content="Unauthorised"
        )

    instance_config = get_instance_config(firm)
    if instance_config is None or instance_config.agresso is None:
        return fastapi.responses.PlainTextResponse(
            status_code=500, content="Internal server error"
        )

    client = DocumentClient(instance_config.agresso.client)
    reply = client.get_document(firm, doc_id, doc_type, revision)

    if reply is None:
        return fastapi.responses.PlainTextResponse(
            status_code=404, content="Document not Found"
        )

    document, filename, mime_type, encoding = reply

    headers = {
        "Content-Disposition": f'inline; filename="{ filename }"',
        "Cache-Control": "no-store",
    }
    if encoding:
        headers.update({"Content-Encoding": encoding})

    return fastapi.responses.Response(
        content=document,
        media_type=mime_type,
        headers=headers,
    )


@app.get("/health")
async def health() -> fastapi.responses.JSONResponse:
    """Health endpoint"""
    return fastapi.responses.JSONResponse(
        {
            "metadata": {
                "updated": time.time_ns() // 1_000_000,  # unix time in milliseconds
                "health-file-version": 3,  # zabbix monitor format version
            },
        }
    )


old_bot = fastapi.FastAPI(docs_url=None, redoc_url=None)


def get_old_bot_document(
    firm: str,
    doc_id: int,
    source_system: oracle_bot.SourceSystem,
) -> fastapi.responses.Response:
    oracle_client = get_oracle_client(firm)
    if oracle_client is None:
        return fastapi.responses.PlainTextResponse(status_code=404)

    match source_system:
        case oracle_bot.SourceSystem.FND_LOBS:
            answer = oracle_client.get_fnd_lobs_document(doc_id)
        case oracle_bot.SourceSystem.STREAMSERVE:
            answer = oracle_client.get_streamserve_document(firm=firm, fakturanr=doc_id)
        case _:
            assert_never(source_system)

    if not answer:
        return fastapi.responses.PlainTextResponse(
            status_code=404, content="Document not found"
        )

    headers = {
        "Cache-Control": "no-store",
    }
    if answer.filename:
        # Stolen from starlette.FileResponse
        content_disposition_filename = urllib.parse.quote(answer.filename)
        if content_disposition_filename != answer.filename:
            content_disposition = "inline; filename*=utf-8''{}".format(
                content_disposition_filename
            )
        else:
            content_disposition = 'inline; filename="{}"'.format(answer.filename)
        headers["Content-Disposition"] = content_disposition

    return fastapi.responses.Response(
        content=answer.document,
        media_type=answer.mimetype,
        headers=headers,
    )


@old_bot.get("/oa")  # oa = Oracle Applications
async def get_fnd_lobs_data(
    firm: str,
    doc_id: int,
    authorised: bool = fastapi.Depends(authorised),
) -> fastapi.responses.Response:
    if not authorised:
        return fastapi.responses.PlainTextResponse(
            status_code=401, content="Unauthorised"
        )
    return get_old_bot_document(firm, doc_id, oracle_bot.SourceSystem.FND_LOBS)


@old_bot.get("/ss")  # ss = Streamserve
async def get_streamserve_data(
    firm: str,
    doc_id: int,
    authorised: bool = fastapi.Depends(authorised),
) -> fastapi.responses.Response:
    if not authorised:
        return fastapi.responses.PlainTextResponse(
            status_code=401, content="Unauthorised"
        )
    return get_old_bot_document(firm, doc_id, oracle_bot.SourceSystem.STREAMSERVE)


app.mount("/gamle-okonomidata", old_bot)
