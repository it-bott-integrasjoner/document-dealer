from typing import Any, Optional, Self, Annotated
import os

import pydantic
import yaml
from pydantic import ConfigDict, BeforeValidator

import document_dealer
import document_dealer.oracle_bot as oracle_bot
import document_dealer.document_client as document_client


class NoConfigException(Exception):
    pass


class AuthConfig(pydantic.BaseModel):
    key: str


class AgressoArchiveConfig(pydantic.BaseModel):
    client: document_client.DocumentClientConfig


class OldBotConfig(pydantic.BaseModel):
    database: oracle_bot.OracleConfig


class InstanceConfig(pydantic.BaseModel):
    auth_config: AuthConfig
    agresso: AgressoArchiveConfig
    old_bot_enabled: bool


class SentryConfig(pydantic.BaseModel):
    dsn: str
    environment: str


class Config(pydantic.BaseModel):
    instance_config: dict[str, InstanceConfig]
    sentry: Optional[SentryConfig] = None
    logging: Optional[dict[Any, Any]] = None
    old_bot: OldBotConfig | None = None

    @pydantic.field_validator("instance_config")
    @classmethod
    def _check_instance_config(
        cls, value: dict[str, InstanceConfig]
    ) -> dict[str, InstanceConfig]:
        if len(value) == 0:
            raise ValueError("can't be empty")
        return value

    @pydantic.model_validator(mode="after")
    def _check_model(self) -> Self:
        if (
            any(x.old_bot_enabled for x in self.instance_config.values())
            and self.old_bot is None
        ):
            msg = "old_bot enabled but not configured"
            raise ValueError(msg)
        return self


def get_config(filename: Optional[str] = None) -> Config:
    filename = filename or os.environ.get("DOCUMENT_DEALER_CONFIG")
    if filename is None:
        raise NoConfigException

    config_file = yaml.load(
        document_dealer.utils.load_file(filename), Loader=yaml.FullLoader
    )

    return Config(**config_file)
