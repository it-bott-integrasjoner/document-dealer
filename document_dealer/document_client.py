import base64
import os
import mimetypes

import pydantic
import requests
import zeep
from typing import Any, Optional, Tuple


class SoapCredentials(pydantic.BaseModel):
    username: str
    password: str


class DocumentClientConfig(pydantic.BaseModel):
    url: pydantic.HttpUrl
    headers: dict[str, str] = {}
    credentials: SoapCredentials


class DocumentClient:
    def __init__(self, config: DocumentClientConfig):
        self.config = config
        self.session = requests.Session()
        self.session.headers.update(config.headers)
        self.transport = zeep.transports.Transport(session=self.session)  # type: ignore[no-untyped-call]
        self.client = zeep.Client(  # type: ignore[no-untyped-call]
            os.path.join(
                os.path.dirname(__file__), "..", "wsdl", "DocArchiveV201409.wsdl"
            ),
            transport=self.transport,
        )
        self.service = self.client.create_service(  # type: ignore[no-untyped-call]
            "{http://services.agresso.com/DocArchiveService/DocArchiveV201409}DocArchiveV201409Soap",
            self.config.url,
        )

    def get_document(
        self, firm: str, doc_id: str, doc_type: str, revision: int
    ) -> Tuple[bytes, Any, Optional[str], Optional[str]] | None:
        reply = self.service.GetDocumentRevision(
            credentials={
                "Username": self.config.credentials.username,
                "Password": self.config.credentials.password,
                "Client": firm,
            },
            request={
                "DocId": doc_id,
                "DocType": doc_type,
                "RevisionNo": revision,
                "PageNo": 0,
            },
        )
        if reply["Revision"]:
            return (
                base64.b64decode(reply["Revision"]["FileContent"]),
                reply["Revision"]["FileName"],
                *mimetypes.guess_type(reply["Revision"]["FileName"]),
            )
        return None
